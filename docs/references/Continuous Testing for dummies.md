---
title: Continuous Testing for dummies
authors: Marianne Hollier, Allan Wagner
year: 2017
tags: #review
annotation-target: Continuous Testing For Dummies.pdf
---
# title  

زمان ایجاد : 20:21 04-11-2022


کلمات کلیدی : #review

## فهرست
```toc
    style: bullet 
    min_depth: 1 
    max_depth: 6 
```
[[_toc_]]

---

## متن سند




![](../attachments/Continuous%20Testing%20For%20Dummies.pdf)




## نکات







##  هایلایت ها و کامنت های در متن (قابل مشاهده با افزونه Annotator)








>%%
>```annotation-json
>{"created":"2022-11-04T16:53:57.782Z","updated":"2022-11-04T16:53:57.782Z","document":{"title":"Continuous Testing For Dummies®, IBM Limited Edition","link":[{"href":"urn:x-pdf:08b349ffab6db0a0c3b92c0cb2e5a983"},{"href":"vault:/docs/attachments/Continuous Testing For Dummies.pdf"}],"documentFingerprint":"08b349ffab6db0a0c3b92c0cb2e5a983"},"uri":"vault:/docs/attachments/Continuous Testing For Dummies.pdf","target":[{"source":"vault:/docs/attachments/Continuous Testing For Dummies.pdf","selector":[{"type":"TextPositionSelector","start":8516,"end":8547},{"type":"TextQuoteSelector","exact":"ery.  Successful  companies  ar","prefix":"ity software  and  faster  deliv","suffix":"e  honing their software develop"}]}]}
>```
>%%
>*%%PREFIX%%ity software  and  faster  deliv%%HIGHLIGHT%% ==ery.  Successful  companies  ar== %%POSTFIX%%e  honing their software develop*
>%%LINK%%[[#^jgwp9kgbztj|show annotation]]
>%%COMMENT%%
>
>%%TAGS%%
>
^jgwp9kgbztj


>%%
>```annotation-json
>{"created":"2022-11-04T16:54:04.665Z","text":"این یک یادداشت است.","updated":"2022-11-04T16:54:04.665Z","document":{"title":"Continuous Testing For Dummies®, IBM Limited Edition","link":[{"href":"urn:x-pdf:08b349ffab6db0a0c3b92c0cb2e5a983"},{"href":"vault:/docs/attachments/Continuous Testing For Dummies.pdf"}],"documentFingerprint":"08b349ffab6db0a0c3b92c0cb2e5a983"},"uri":"vault:/docs/attachments/Continuous Testing For Dummies.pdf","target":[{"source":"vault:/docs/attachments/Continuous Testing For Dummies.pdf","selector":[{"type":"TextPositionSelector","start":9097,"end":9127},{"type":"TextQuoteSelector","exact":"M DevOps approach is to accele","prefix":"quality.The goal of taking an IB","suffix":"rate soft-ware delivery while at"}]}]}
>```
>%%
>*%%PREFIX%%quality.The goal of taking an IB%%HIGHLIGHT%% ==M DevOps approach is to accele== %%POSTFIX%%rate soft-ware delivery while at*
>%%LINK%%[[#^66qmy0l4lxh|show annotation]]
>%%COMMENT%%
>این یک یادداشت است.
>%%TAGS%%
>
^66qmy0l4lxh
